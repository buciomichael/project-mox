import React, {useEffect, useState} from 'react'
import { Link } from 'react-router-dom';
import './ProductList.css';
import img1 from './images/advertisement-1.jpg'
import Nav from './Nav';

function ProductList() {

    const [products, setProducts] = useState([]);
    const [photos, setPhotos] = useState([]);

    async function fetchData() {
        const url = 'http://127.0.0.1:8000/products/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setProducts(data.products)
        }
    };

    useEffect(() => {
        fetchData();
    }, [])

    return (
      <>
      <div className='main-grid-list-container'>
        <div className='nav-list'><Nav /></div>
        <div className='product-list-container'>
            <div className='side-navigation'>
                <ul>
                <div className='browse'>
                  <li>Browse by:</li>
                </div>
                <li>All Products</li>
                <li>Kitchen</li>
                <li>Bathroom</li>
                <li>Cleaning</li>
                <li>Outdoors</li>
                </ul>
            </div>
            <div>
              <section className='list-main-1'>
                        <div className='list-main-1-prd'>
                            <div>
                            <img className src={img1}></img>
                            </div>
                        </div>
                {products.map(product => {
                  if(product.sku === 'PRD0' || product.sku === 'PRD2') {
                    return (
                        <div className='list-main-1-prd'>
                          <div>
                            <img className='list-img' src={product.imageURL}></img>
                          </div>
                          <div className='product-name-container'>
                            <Link className='product-name' to={`/products/${product.id}`}>{product.name}</Link>
                          </div>
                          <div>
                            <span className='product-price'>{product.price} USD</span>
                          </div>
                        </div>
                      );
                  }
              })}
              </section>
              <section className='list-middle'>
                {products.map(product => {
                  if(product.sku === 'PRD3' || product.sku === 'PRD4' || product.sku === 'PRD5' || product.sku === 'PRD6') {
                    return (
                        <div className='product-middle'>
                          <div>
                            <img src={product.imageURL}></img>
                          </div>
                          <div className='product-name-container'>
                            <Link className='product-name' to={`/products/${product.id}`}>{product.name}</Link>
                          </div>
                          <div>
                            <span className='product-price'>{product.price} USD</span>
                          </div>
                        </div>
                      );
                  }
              })}
              </section>
              <section className='list-main-2'>
                {products.map(product => {
                  if(product.sku === 'PRD7' || product.sku === 'PRD8' || product.sku === 'PRD9') {
                    return (
                      <div className='list-main-2-prd'>
                          <div>
                            <img src={product.imageURL}></img>
                          </div>
                          <div className='product-name-container'>
                            <Link className='product-name' to={`/products/${product.id}`}>{product.name}</Link>
                          </div>
                          <div>
                            <span className='product-price'>{product.price} USD</span>
                          </div>
                        </div>
                      );
                  }
              })}
              </section>
          </div>
          </div>
      </div>
    </>
    )

}

export default ProductList
