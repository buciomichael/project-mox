import React, {useEffect, useState} from 'react'
import { useParams } from 'react-router-dom';
import './ProductPage.css';
import img from './images/product_images/pink_products.jpg'
import img1 from './images/product_images/brush1.jpg'
import img2 from './images/product_images/brush2.jpg'
import img3 from './images/product_images/brush3.jpg'
import Nav from './Nav';


function ProductPage() {


    const [product, setProducts] = useState([]);
    const productparam = useParams();
    console.log(productparam)

    async function fetchData() {
        const url = `http://127.0.0.1:8000/products/${productparam.id}`;

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setProducts(data.product)
        }
    };

    useEffect(() => {
        fetchData();
    }, [])



  return (
    <div className='product-main-grid'>
        <div className='product-page-nav'><Nav /></div>
        <div className='product-detail-container'>
            <div className='side-img-container'>
                <div className='side-1'>
                    <img className='product-img-side img-1' src={img1}></img>
                </div>
                <div className='side-2'>
                    <img className='product-img-side img-2' src={img2}></img>
                </div>
                <div className='side-3'>
                    <img className='product-img-side img-3' src={img3}></img>
                </div>
            </div>
            <div className='main-img-container'>
                <img className='product-img-main' src={img}></img>
            </div>
            <div className='product-description-container'>
                <h2 className='product-detail-name'>{product.name}</h2>
                <p className='product-detail-price'>${product.price}</p>
                <div className='qty-add-container'>
                    <div className='quantity-counter'>
                        <button className='increment-button'>-</button>
                        <span>0</span>
                        <button className='decrement-button'>+</button>
                    </div>
                    <button className='add-to-bag'>Add to Bag</button>
                </div>
                <div className='product-details'>
                    <div className='details'>
                        <span>Details</span>
                        <span className='minus'>-</span>
                    </div>
                        <p>
                            Crafted from natural and sustainable materials, this brush is designed to tackle dirt,
                            grime, and stains effectively while minimizing your environmental footprint.
                        </p>
                        <p className='content-care'>Content + Care</p>
                        <p>
                            - Not Dishwaser Safe
                            <br></br>
                            - Renewable Plant Fibers
                        </p>
                </div>
                <div className='shipping-returns'>
                    <span>Shipping + Returns</span>
                    <span className='plus'>+</span>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ProductPage
