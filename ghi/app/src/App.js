import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Banner from './Banner';
import LeftNav from './LeftNav';
import Nav from './Nav';
import ProductList from './ProductList';
import ProductPage from './ProductPage';


function App() {
  return (
    <>
    <BrowserRouter>
      <Banner />
      <Routes>
        {/* <Route index path="/" element={<Home />}></Route> */}
        <Route path="products">
          <Route index element={<ProductList />}></Route>
          {/* <Route path="kitchen" element={<Kitchen />} /> */}
        </Route>
        <Route path='products/:id' element={<ProductPage />}></Route>
      </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;
