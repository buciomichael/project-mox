import React from 'react'
import './LeftNav.css';

function LeftNav() {
  return (
    <div className='left-nav-container'>
      <ul>
        <div className='browse'>
          <li>Browse by:</li>
        </div>
        <li>All Products</li>
        <li>Kitchen</li>
        <li>Bathroom</li>
        <li>Cleaning</li>
        <li>Outdoors</li>
      </ul>
    </div>
  )
}

export default LeftNav
