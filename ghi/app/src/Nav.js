import './Nav.css';
import { NavLink } from 'react-router-dom';



function Nav() {
  return (
    <div className='main-nav'>
      <div className='logo'>MOX</div>
      <div>
        <ul className='main-nav-links'>
          <li>
            <NavLink to='/products'>SHOP</NavLink>
          </li>
          <li><a href=''>ABOUT US</a></li>
          <li><a href=''>CONTACT</a></li>
          <li><a href=''>LOCATE US</a></li>
        </ul>
      </div>
      <div className='shopping-cart'>
        <svg className='cart' xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-bag-fill" viewBox="0 0 16 16">
          <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z"/>
        </svg>
      </div>
    </div>
  )
}

export default Nav
