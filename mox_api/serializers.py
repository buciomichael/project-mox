from rest_framework import serializers
from store.models import Product


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'description', 'sku')
