from rest_framework.response import Response
from rest_framework.decorators import api_view
from store.models import Product
from mox_api.serializers import ItemSerializer


@api_view(['GET'])
def getProducts(request):
    if request.method == "GET":
        products = Product.objects.all()
        serializer = ItemSerializer(products, many=True)
    return Response({"products": serializer.data})


@api_view(['GET'])
def getProduct(request, id):
    if request.method == "GET":
        product = Product.objects.get(id=id)
        serializer = ItemSerializer(product)
    return Response({"product": serializer.data})
